<?php

/**
 * Class ModelShippingBring
 */
class ModelShippingBring extends Model {

    /**
     * Errors which can be displayed in the frontend, to the user
     * * NB: Displaying the error will kill any other valid shipping option/service
     * @var string
     */
    public $error = '';


    /**
     * Holds the returned pickup points from $this->generatePickupPoints
     * array(10) { [0]=> ... details about the first pickup point [1] => etc
     * @var array
     */
    public $pickupPoints = '';


    public $taxClass = 0;
    public $geoZone = 0;
    public $shippingPrice = 0;
    public $shippingPriceMax = 0;
    public $shippingPriceMin = 0;


    /**
     * Requests 10 pickup points from Bring API
     * Checks whether the country is supported & the postal code is correct
     * @param $countryCode
     * @param $postcode
     * @return string error message
     */
    public function generatePickupPoints($countryCode, $postcode) {

        $postcode = filter_var($postcode, FILTER_SANITIZE_NUMBER_INT);
        $countryCode = filter_var($countryCode, FILTER_SANITIZE_STRING);
        $availableCountries = array('NO', 'DK', 'SE', 'FI');

        if (in_array($countryCode, $availableCountries) && $this->validatePostalCode($countryCode, $postcode)) {

            // We have a valid postal code and the country is supported - we can display 10 pickup points
            // Get cURL resource
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.bring.com/pickuppoint/api/pickuppoint/{$countryCode}/postalCode/{$postcode}.json",
                CURLOPT_USERAGENT => 'cURL'
            ));
            // Send the request & save response
            $response = curl_exec($curl);
            curl_close($curl);
            // Return an array this time
            $formattedResponse = json_decode($response, true);
            // un-nest a little
            $this->pickupPoints = $formattedResponse['pickupPoint']; // 10 arrays (pickup points)

        } else {
            // TODO: remove
            $this->pickupPoints = 'Pickup Points are not available for this country.';
        }

    }


    /**
     * Checks if the country is supported by Bring +
     * Checks if the postal code is correct
     * @param $countryCode
     * @param $postcode
     * @return string city | bool false
     */
    public function validatePostalCode($countryCode, $postcode) {

        $postcode = filter_var($postcode, FILTER_SANITIZE_NUMBER_INT);
        $countryCode = filter_var($countryCode, FILTER_SANITIZE_STRING);
        $availableCountries = array('NO', 'DK', 'SE', 'FI', 'NL', 'DE', 'US', 'BE', 'FO', 'GL');

        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.bring.com/shippingguide/api/postalCode.json?clientUrl={$this->config->get('bring_site_url')}&country={$countryCode}&pnr={$postcode}",
            CURLOPT_USERAGENT => 'cURL'
        ));
        // Send the request & save response
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);

        // For unsupported countries (countries not in the list above),
        // all postal codes will be marked as valid with no city name returned.
        if (in_array($countryCode, $availableCountries) && $response->valid) {
            // Return the city for this postal code
            return $response->result;
        } else {
            return 'Invalid Postal Code.';
        }


    }


    /**
     * Sets the tax class id for a specific country
     * @param $countryId
     */
    public function setTaxClass($countryId) {

        // SELECT @geoZoneId := (SELECT geo_zone_id FROM oc_zone_to_geo_zone WHERE country_id = 81);
        // SELECT * FROM oc_setting WHERE `key` = CONCAT('bring_tax_class_id_',(SELECT geo_zone_id FROM oc_zone_to_geo_zone WHERE country_id = 81));
        $countryId = (int) $countryId;
        $queryForTaxClass = $this->db->query("
          SELECT * FROM `".DB_PREFIX."setting`
          WHERE `key` = CONCAT(
                                'bring_tax_class_id_',
                                (SELECT geo_zone_id FROM ".DB_PREFIX."zone_to_geo_zone WHERE country_id = {$countryId} LIMIT 1)
                                )
          LIMIT 1
        ");

        // Array ( [0] => Array ( [setting_id] => 263 [store_id] => 0 [group] => bring
        // [key] => bring_tax_class_id_5
        // [value] => 10 [serialized] => 0 ) )
        $taxClass = $queryForTaxClass->rows;

        // An empty tax class means no extra taxes
        if (!empty($taxClass)) {
            $this->taxClass = $taxClass[0]['value'];
        }
    }


    /**
     * Set the numerical value of the geo zone for the current address (based on country id)
     * @param $countryId
     */
    public function setGeoZone($countryId) {
        $countryId = (int) $countryId;
        $query = $this->db->query("SELECT geo_zone_id FROM ".DB_PREFIX."zone_to_geo_zone WHERE country_id = '{$countryId}' LIMIT 1");
        $geoZone = $query->rows;

        if ($geoZone) {
            $this->geoZone = $geoZone[0]['geo_zone_id'];
        }
    }


    /**
     * Set the price for this shipping option - pull from db based on geo zone
     */
    public function setShippingPrice() {
        $this->shippingPrice = $this->config->get("bring_{$this->geoZone}_price");
    }


    /**
     * Set the min cart value pulled from db based on geo zone
     */
    public function setShippingPriceMax() {
        $this->shippingPriceMax = $this->config->get("bring_{$this->geoZone}_max_price");
    }


    /**
     * Set the min cart value pulled from db based on geo zone
     */
    public function setShippingPriceMin() {
        $this->shippingPriceMin = $this->config->get("bring_{$this->geoZone}_min_price");
    }

    /*
     * $this->cart->getSubTotal()
     */

    /**
     * Return shipping options/services back into frontend
     * @param $address
     * @return array
     */
    public function getQuote($address) {

        /* Lets fix those horrible names
        [firstname] => Gigi
        [lastname] => Gigi
        [company] =>
        [address_1] => Stromstrade 18
        [address_2] =>
        [postcode] => 1337
        [city] => Berlin
        [country_id] => 81
        [zone_id] => 1256
        [country] => Germany
        [iso_code_2] => DE
        [iso_code_3] => DEU
        [zone] => Berlin
        [zone_code] => BER
        */

        // Store the full address
        $fullAddress = $address['address_1'] . (isset($address['address_2']) ? ' ' . $address['address_2'] : '');
        $postalcode = (isset($address['postcode'])) ? (int) $address['postcode'] : 0;
        $countryId = (int) $address['country_id'];
        // DE, NL, RO etc.
        $countryCode = $address['iso_code_2'];
        // City ID
        $cityId = (int) $address['zone_id'];
        // City name
        $cityName = $address['zone'];
        // City code: BER, BUC
        $cityCode = $address['zone_code'];

        // Pull in any translation
        $this->language->load('shipping/bring');
        // Methods: getCountry($countryId), getCountries()
        $this->load->model('localisation/country');
        // Methods: getZone($zone_id), getZonesByCountryId($countryId)
        $this->load->model('localisation/country');

        // Load the geo zones and its taxes based on the address
        $this->setGeoZone($countryId);
        $this->setTaxClass($countryId);

        // Generate pickup points
        $this->generatePickupPoints($countryCode, $postalcode);

        // Set the prices
        $this->setShippingPrice();
        $this->setShippingPriceMax();
        $this->setShippingPriceMin();

        // Store in $_SESSION data that we will be using in the success controller to query bring again
        $this->session->data['bringCountryCode'] = $countryCode;
        $this->session->data['bringPostalCode'] =  $postalcode;

//        $this->load->model('localisation/tax_class');
//        $this->load->model('localisation/geo_zone');
//        $geo_zones = $this->model_localisation_geo_zone->getGeoZones();
//        $tax_classes = $this->model_localisation_tax_class->getTaxClasses();

        // TODO: remove
//        echo '<pre>';
//        print_r($address);
//        echo '----------' . PHP_EOL;
//        echo 'Tax class id: ' . $this->taxClass . PHP_EOL;
//        echo 'Geo zone id: ' . $this->geoZone . PHP_EOL;
//        echo 'Shipping price: ' . $this->shippingPrice . PHP_EOL;
//        echo 'Max price: ' . $this->shippingPriceMax . PHP_EOL;
//        echo 'Min price: ' . $this->shippingPriceMin . PHP_EOL;
//        //echo 'Pickup points available? ' . print_r($this->pickupPoints, true);
//        echo '</pre>' . PHP_EOL;



//        // Shipping option / service
//        $quote_data['bring1'] = array(
//            'code'         => 'bring.bring1',
//            'title'        => 'Bring - Pickup Point',
//            'cost'         => '133',
//            'tax_class_id' => $this->config->get('bring_tax_class_id_1'),
//            'text'         => $this->currency->format($this->tax->calculate('133', $this->config->get('bring_tax_class_id_1'), $this->config->get('config_tax')))
//        );

        /*
         * Display pickup points
         *
         * if $this->shippingPrice is not set, it means that this geo zone is not configured
         */
        if (is_array($this->pickupPoints) && !empty($this->pickupPoints) && !empty($this->shippingPrice)) {


            foreach ($this->pickupPoints as $pickupPoint => $details) {
                $quote_data['bring_pickup_point_' . $details['id']] = array(
                    'code' => 'bring.bring_pickup_point_' . $details['id'],
                    'title' => "{$details['name']}, City: {$details['city']}, Address: {$details['address']}, Postal Code: {$details['postalCode']}, Open [{$details['openingHoursNorwegian']}], <a href='{$details['googleMapsLink']}' target='_blank'>View on Google Maps</a> ",
                    'cost' => $this->shippingPrice,
                    'tax_class_id' => $this->taxClass,
                    'text' => $this->currency->format($this->tax->calculate($this->shippingPrice, $this->taxClass, $this->config->get('config_tax')))
                );
            }

            // Shipping method
            $method_data = array(
                'code' => 'bring',
                'title' => 'Bring Pickup Points',
                'quote' => $quote_data,
                'sort_order' => $this->config->get('bring_sort_order'),
                'error' => $this->error
            );

            return $method_data;

        }

    }

}